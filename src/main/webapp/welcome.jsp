<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Страница пользователя</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<header>
    <h1>Прокат велосипедов</h1>
</header>
<main>
    <h2>Аккаунт</h2>
    <a href="#" class="mdc-button mdc-button--raised">
        <span class="mdc-button__label">Добавить</span>
        <i class="material-icons mdc-button__icon" aria-hidden="true">add_circle_outline</i>
    </a>
    <a href="#" class="mdc-button mdc-button--raised">
        <span class="mdc-button__label">Удалить</span>
        <i class="material-icons mdc-button__icon" aria-hidden="true">delete</i>
    </a>
    <table class="mdc-data-table">
        <thead>
        <tr class="mdc-data-table__header-row">
            <th class="mdc-data-table__header-cell" role="columnheader" scope="col">Название</th>
            <th class="mdc-data-table__header-cell" role="columnheader" scope="col">Изображение</th>
            <th class="mdc-data-table__header-cell" role="columnheader" scope="col">Описание</th>
        </tr>
        </thead>
        <tbody class="mdc-data-table__content">
        <tr class="mdc-data-table__row">
            <td class="mdc-data-table__cell">Товар 1</td>
            <td class="mdc-data-table__cell"><img src="https://upload.wikimedia.org/wikipedia/commons/3/3e/Tree-256x256.png" alt="Изображение 1"></td>
            <td class="mdc-data-table__cell">Описание товара 1</td>
        </tr>
        <tr class="mdc-data-table__row">
            <td class="mdc-data-table__cell">Товар 2</td>
            <td class="mdc-data-table__cell"><img src="https://www.iconexperience.com/_img/v_collection_png/256x256/shadow/tree.png" alt="Изображение 2"></td>
            <td class="mdc-data-table__cell">Описание товара 2</td>
        </tr>
        <tr class="mdc-data-table__row">
            <td class="mdc-data-table__cell">Товар 3</td>
            <td class="mdc-data-table__cell"><img src="https://icons.iconseeker.com/png/fullsize/circle-feeds/feeds-green-256x256.png" alt="Изображение 3"></td>
            <td class="mdc-data-table__cell">Описание товара 3</td>
        </tr>
        </tbody>
    </table>
</main>
<footer>
    <p>&copy; 2023 Прокат велосипедов</p>
</footer>
<script src="https://unpkg.com/material-components-web@13.0.0/dist/material-components-web.min.js"></script>
<script>
    mdc.autoInit();
</script>
</body>
</html>